const mongoose = require('mongoose');
require('dotenv').config();

const User = require('./models/User');
const DepositWithdrawTransaction = require('./models/DepositWithdrawTransaction');
const Account = require('./models/Account');
const TransferTransaction = require('./models/TransferTransaction');

mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const usersData = [
  {
    username: 'admin',
    password: 'adminpassword',
    role: 'admin',
    email: 'admin@example.com',
  },
  {
    username: 'user1',
    password: 'user1password',
    role: 'user',
    email: 'user1@example.com',
  },
  {
    username: 'user2',
    password: 'user2password',
    role: 'user',
    email: 'user1@example.com',
  },
  // Add more user data as needed
];

const depositWithdrawTransactionsData = [
  {
    id: 1,
    account_id: 'account1',
    target_exchain_address: 'BTC_address',
    target_bank_account: 'Bank_account1',
    status: 'verifying',
    reason: '',
    amount: 1000,
    type: 'onchain',
  }
  // Add more deposit/withdraw transaction data as needed
];

const accountsData = [
  {
    id: 1,
    username: 'user1',
    exchain: 'BTC',
    balance: 10000,
  },
  {
    id: 2,
    username: 'user1',
    exchain: 'ETH',
    balance: 5000,
  },
  {
    id: 3,
    username: 'user2',
    exchain: 'ETH',
    balance: 0,
  },
  // Add more account data as needed
];

const transferTransactionsData = [
  {
    sender_account_id: 2,
    receiver_account_id: 3,
    amount: 1000,
  },
  // Add more transfer transaction data as needed
];

// Define a function to delete all documents from a collection
const deleteAllDocuments = (Model) => {
  return Model.deleteMany({});
};

// Create an array of promises to delete all documents from each collection
const deletePromises = [
  deleteAllDocuments(User),
  deleteAllDocuments(DepositWithdrawTransaction),
  deleteAllDocuments(Account),
  deleteAllDocuments(TransferTransaction),
];

// Use Promise.all to delete all documents before inserting new data
Promise.all(deletePromises)
  .then(() => {
    // After all data is deleted, proceed with inserting new data
    const insertPromises = [
      User.insertMany(usersData),
      DepositWithdrawTransaction.insertMany(depositWithdrawTransactionsData),
      Account.insertMany(accountsData),
      TransferTransaction.insertMany(transferTransactionsData),
    ];

    return Promise.all(insertPromises);
  })
  .then(() => {
    console.log('All data seeded successfully');
    mongoose.connection.close();
  })
  .catch((err) => {
    console.error('Error seeding data:', err);
    mongoose.connection.close();
  });
