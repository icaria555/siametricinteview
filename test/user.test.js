const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server'); // Import your Express app

// Configure Chai to use chai-http
chai.use(chaiHttp);
const expect = chai.expect;

describe('User API Tests', () => {
  it('should register a new user', (done) => {
    chai
      .request(app)
      .post('/register')
      .send({ username: 'testuser', password: 'password', role: 'USER', email: 'test@example.com' })
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.body.message).to.equal('User registered successfully');
        done();
      });
  });

  it('should not allow duplicate registration', (done) => {
    chai
      .request(app)
      .post('/register')
      .send({ username: 'testuser', password: 'password', role: 'USER', email: 'test@example.com' })
      .end((err, res) => {
        expect(res).to.have.status(500); // You can adjust the expected status code as needed
        expect(res.body.error).to.equal('Registration failed');
        done();
      });
  });

  // Add more test cases for other API endpoints
});
