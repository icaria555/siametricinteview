const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
  id: { type: Number, required: true, unique: true },
  username: { type: String },
  exchain: { type: String, enum: ['BTC', 'SOL', 'ETH'] },
  balance: { type: Number, required: true },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Account', accountSchema);
