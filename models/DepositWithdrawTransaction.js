const mongoose = require('mongoose');

const depositWithdrawTransactionSchema = new mongoose.Schema({
  id: { type: Number, required: true, unique: true },
  account_id: { type: String, required: true },
  target_exchain_address: { type: String },
  target_bank_account: { type: String },
  status: {
    type: String,
    enum: ['verifying', 'rejected', 'successed'],
    required: true,
  },
  reason: { type: String },
  amount: { type: Number, required: true },
  type: {
    type: String,
    enum: ['onchain', 'onbankaccount'],
    required: true,
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('DepositWithdrawTransaction', depositWithdrawTransactionSchema);
