const mongoose = require('mongoose');

const transferTransactionSchema = new mongoose.Schema({
  sender_account_id: { type: Number, required: true },
  receiver_account_id: { type: Number, required: true },
  amount: { type: Number, required: true },
  created_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('TransferTransaction', transferTransactionSchema);
